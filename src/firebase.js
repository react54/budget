import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB3sJHcQf-4cxxjiOfQ9Z_aBedGuEqci3A",
  authDomain: "expensify-95a7c.firebaseapp.com",
  databaseURL: "https://expensify-95a7c.firebaseio.com",
  projectId: "expensify-95a7c",
  storageBucket: "expensify-95a7c.appspot.com",
  messagingSenderId: "59504913585",
  appId: "1:59504913585:web:b30a2f2575ebb219ebc690",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();

export { firebase, auth, db };
