import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import TransactionProvider from "./context/TransactionProvider";
import UserProvider from "./context/UserProvider";

ReactDOM.render(
  <UserProvider>
    <TransactionProvider>
      <App />
    </TransactionProvider>
  </UserProvider>,
  document.getElementById("root")
);
