import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Login from "./auth/Login";
import { auth } from "./firebase";
import Admin from "./Components/Admin";
import Navbar from "./Components/Navbar";
import NotFoundPage from "./Components/NotFoundPage";

function App() {
  const [firebaseUser, setFirebaseUser] = React.useState(false);
  React.useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        setFirebaseUser(user);
      } else {
        setFirebaseUser(null);
      }
    });
  }, []);

  return firebaseUser !== false ? (
    <Router>
      <Navbar firebaseUser={firebaseUser}></Navbar>
      <Switch>
        <Route component={Admin} path="/" exact={true}></Route>
        <Route component={Login} path="/login" exact></Route>
        <Route component={Admin} path="/admin" exact></Route>
        <Route component={NotFoundPage}></Route>
      </Switch>
    </Router>
  ) : (
    <div> Loading...........</div>
  );
}

export default App;
