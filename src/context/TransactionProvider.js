import React from "react";

import { db } from "../firebase";
import { UserContext } from "../context/UserProvider";

export const TransactionContext = React.createContext();

const TransactionProvider = (props) => {
  const { data, setData, currentUserData } = React.useContext(UserContext);

  const [error, setError] = React.useState(null);

  /// Add Transaction

  const addTransaction = async (date, concept, amount, type) => {
    if (!date.trim()) {
      setError("Date field is empty");
      return;
    } else if (!concept.trim()) {
      setError("Concept field  is empty");
      return;
    } else if (amount === null || amount === "") {
      setError("Amount field  is empty");
      return;
    } else if (!type.trim()) {
      setError("Type field  is empty");
      return;
    }

    try {
      const newTran = {
        date,
        concept,
        type,
        amount,
      };
      const nData = await db.collection(currentUserData.email).add(newTran);
      setData([
        ...data,
        {
          ...newTran,
          id: nData.id,
        },
      ]);
      setError(null);
    } catch (error) {
      console.log(error);
    }
  };

  /// Delete Transaction

  const deleteTransaction = async (id) => {
    try {
      await db.collection(currentUserData.email).doc(id).delete();
      const fileteredTransaction = data.filter((tran) => tran.id !== id);
      setData(fileteredTransaction);
    } catch (error) {
      console.log(error);
    }
  };

  ///Edit Transaction

  const editTransaction = async (id, date, concept, amount, type) => {
    if (!date.trim()) {
      setError("Date field is empty");
      return;
    } else if (!concept.trim()) {
      setError("Concept field  is empty");
      return;
    } else if (amount === null || amount === "") {
      setError("Amount field  is empty");
      return;
    } else if (!type.trim()) {
      setError("Type field  is empty");
      return;
    }

    try {
      const editedTran = {
        id,
        date,
        concept,
        type,
        amount,
      };
      await db.collection(currentUserData.email).doc(id).update(editedTran);
      const findValue = data.map((tran) =>
        tran.id === id
          ? {
              id,
              date,
              concept,
              type,
              amount,
            }
          : tran
      );
      setData(findValue);
      setError(null);
    } catch (error) {
      console.log(error);
    }
  };

  /// Search

  const [filter, setFilter] = React.useState("");
  const [searched, setSearched] = React.useState([]);
  const [type, setType] = React.useState("");

  React.useEffect(() => {
    if (type === "type") {
      setSearched(
        data.filter((t) => {
          return t.type.toLowerCase().includes(filter.toLowerCase());
        })
      );
      return;
    } else if (type === "concept") {
      setSearched(
        data.filter((t) => {
          return t.concept.toLowerCase().includes(filter.toLowerCase());
        })
      );
      return;
    }

    setSearched(data);
  }, [filter, data, type]);

  return (
    <TransactionContext.Provider
      value={{
        data,
        setData,
        addTransaction,
        deleteTransaction,
        editTransaction,
        setSearched,
        filter,
        setFilter,
        searched,
        error,
        type,
        setType,
      }}
    >
      {props.children}
    </TransactionContext.Provider>
  );
};

export default TransactionProvider;
