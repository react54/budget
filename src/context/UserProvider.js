import React from "react";
import { auth, firebase, db } from "../firebase";
// import moment from "moment";
// import "moment/locale/es";

export const UserContext = React.createContext();

const UserProvider = (props) => {
  const [data, setData] = React.useState([]);

  const dataUser = { uid: null, email: null, estado: null, displayName: null };
  const [user, setUser] = React.useState(dataUser);

  // const dataTranUser = JSON.parse(localStorage.getItem("data"));

  // React.useEffect(() => {
  //   localStorage.setItem("data", JSON.stringify(data));
  // });

  // const [userTranData, setUserTranData] = React.useState(dataTranUser);

  const [stateData, setStateData] = React.useState([]);

  React.useEffect(() => {
    detectarUsuario();
  }, []);

  //// Detect User

  const detectarUsuario = () => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUser({
          uid: user.uid,
          email: user.email,
          displayName: user.displayName,
          estado: true,
        });
      } else {
        setUser({ uid: null, email: null, estado: false, displayName: null });
      }
    });
  };

  const signIn = async () => {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const res = await auth.signInWithPopup(provider);
      await db.collection(res.user.uid).add({
        date: "2020-07-14",
        concept: "Quincena",
        amount: 10000,
        type: "income",
      });
    } catch (error) {
      console.log(error);
    }
  };

  const [currentUserData, setCurrentUserData] = React.useState(null);

  return (
    <UserContext.Provider
      value={{
        data,
        setData,
        user,
        signIn,
        currentUserData,
        setCurrentUserData,
        stateData,
        setStateData,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export default UserProvider;
