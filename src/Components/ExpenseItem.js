import React from "react";
import { TransactionContext } from "../context/TransactionProvider";
// import { UserContext } from "../context/UserProvider";

import numeral from "numeral";

const ExpenseItem = (props) => {
  const { deleteTransaction, searched } = React.useContext(TransactionContext);

  // const { data } = React.useContext(UserContext);

  //// Get Data from Firestore

  const editRow = (data) => {
    props.setEditing(true);
    props.setCurrentTran({
      order: data.order,
      id: data.id,
      date: data.date,
      concept: data.concept,
      type: data.type,
      amount: data.amount,
    });
  };

  return (
    <tbody>
      {searched.map((row) => (
        <tr key={row.id}>
          <td className={row.type === "expense" ? "expense" : "income"}>
            {row.date}
          </td>
          <td className={row.type === "expense" ? "expense" : "income"}>
            {row.concept}
          </td>
          <td className={row.type === "expense" ? "expense" : "income"}>
            {numeral(row.amount).format("$0,0.00")}
          </td>
          <td className={row.type === "expense" ? "expense" : "income"}>
            {row.type}
          </td>
          <td>
            <button
              className="btn btn-sm btn-danger mx-2"
              onClick={() => deleteTransaction(row.id)}
            >
              Delete
            </button>
            <button
              onClick={() => editRow(row)}
              className="btn btn-sm btn-primary "
            >
              Edit
            </button>
          </td>
        </tr>
      ))}
    </tbody>
  );
};

export default ExpenseItem;
