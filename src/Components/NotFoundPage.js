import React from "react";
import { Link } from "react-router-dom";

const NotFoundPage = () => (
  <div className="notfound">
    <div className="container">
      <div className="row ">
        <div className="col-12">
          <h1 className="text-center mt-5">Expensify</h1>
          <div className="text-center ">
            Not Found Page 404 - <Link to="/">Go home</Link>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default NotFoundPage;
