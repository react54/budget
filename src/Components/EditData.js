import React, { useState } from "react";
import { TransactionContext } from "../context/TransactionProvider";

const EditData = (props) => {
  const { editTransaction, error } = React.useContext(TransactionContext);

  const [id] = useState(props.currentTran.id);

  const [date, setDate] = useState(props.currentTran.date);
  const [concept, setConcept] = useState(props.currentTran.concept);
  const [amount, setAmount] = useState(props.currentTran.amount);
  const [type, setType] = useState(props.currentTran.type);

  const updateUser = (e) => {
    e.preventDefault();
    props.setEditing(false);
    if (type === "expense") {
      editTransaction(id, date, concept, -amount, type);

      setDate(" ");
      setConcept(" ");
      setAmount("");
      setType("");
      return;
    } else if (type === "income") {
      editTransaction(id, date, concept, amount, type);
      setDate(" ");
      setConcept(" ");
      setAmount("");
      setType("");
      return;
    }
  };

  return (
    <div className="mt-5">
      <h4 className="text-center mb-4">Edit Transaction</h4>
      <div className="row text-center mb-3">
        <div className="col">
          {error ? <span className="text-danger">{error}</span> : null}
        </div>
      </div>
      <form onSubmit={updateUser}>
        <div className="row">
          <div className="col">
            <input
              type="date"
              className="form-control mb-2"
              placeholder=" Enter Date"
              name="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Enter Concept"
              name="concept"
              value={concept}
              onChange={(e) => setConcept(e.target.value)}
            />
          </div>

          <div className="col">
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Enter Amount"
              name="amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>

          <div className="col">
            <select
              id="inputState"
              className="form-control"
              name="type"
              value={type}
              onChange={(e) => setType(e.target.value)}
            >
              <option defaultValue="Choose">Choose...</option>
              <option>expense</option>
              <option>income</option>
            </select>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-3 ">
            <button
              className="btn btn-primary btn-block mt-1"
              type="submit"
              // value="addTransaction"
            >
              Edit Transaction
            </button>
            <button
              className="btn btn-danger btn-block mt-1"
              type="submit"
              // value="addTransaction"
              onClick={() => props.setEditing(false)}
            >
              Cancel
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditData;
