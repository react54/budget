import React from "react";
import { Link, NavLink } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { auth } from "../firebase";

import { UserContext } from "../context/UserProvider";

const Navbar = (props) => {
  const { user } = React.useContext(UserContext);

  const cerrarSesion = () => {
    auth.signOut().then(() => {
      props.history.push("/login");
    });
  };

  return (
    <div className="navbar navbar-dark bg-dark">
      <Link className="navbar-brand" to="/admin">
        BUDGET APP
      </Link>
      <div className="d-flex">
        {props.firebaseUser !== null ? (
          <NavLink className="btn btn-dark mr-2 ml-2" to="/admin" exact>
            {user.displayName}
          </NavLink>
        ) : null}

        {props.firebaseUser !== null ? (
          <button onClick={() => cerrarSesion()} className="btn btn-dark mr-2">
            Log out
          </button>
        ) : (
          <NavLink className="btn btn-dark mr-2" to="/login" exact>
            Log in
          </NavLink>
        )}
      </div>
    </div>
  );
};

export default withRouter(Navbar);
