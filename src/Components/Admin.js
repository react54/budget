import React from "react";
import { auth } from "../firebase";
import { withRouter } from "react-router-dom";
import FireStore from "./FireStore";
import { UserContext } from "../context/UserProvider";

const Admin = (props) => {
  const { currentUserData, setCurrentUserData } = React.useContext(UserContext);

  React.useEffect(() => {
    if (auth.currentUser) {
      setCurrentUserData(auth.currentUser);
    } else {
      props.history.push("/login");
    }
  }, [props.history, setCurrentUserData]);

  return <div>{currentUserData && <FireStore></FireStore>}</div>;
};

export default withRouter(Admin);
