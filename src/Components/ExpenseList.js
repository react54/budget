import React from "react";

import { UserContext } from "../context/UserProvider";
import { db } from "../firebase";
import ExpenseItem from "./ExpenseItem";

const ExpenseList = ({ setCurrentTran, setEditing }) => {
  const { currentUserData, setData, setUserTranData } = React.useContext(
    UserContext
  );
  // const [state, setstate] = useState(csvData);

  React.useEffect(() => {
    const getData = async () => {
      try {
        const data = await db
          .collection(currentUserData.email)
          .orderBy("date")
          .get();
        // console.log(data);

        const arrayData = data.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));

        setData(arrayData);
      } catch (error) {
        console.log(error);
      }
    };
    getData();
  }, [currentUserData.email, setData, setUserTranData]);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">Date</th>
          <th scope="col">Concept</th>
          <th scope="col">Amount</th>
          <th scope="col">Type(exp/inc)</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>

      <ExpenseItem
        setCurrentTran={setCurrentTran}
        setEditing={setEditing}
      ></ExpenseItem>
    </table>
  );
};

export default ExpenseList;

// const objectToCsv = (arrayData) => {
//   const csvRows = [];

//   //   get the headers

//    const headers = Object.keys(arrayData[0]);
//    csvRows.push(headers.join(","));

//   //   //loop over the rows

//     for (const row of arrayData) {
//       const values = headers.map((header) => {
//         const escaped = (" " + row[header]).replace(/"/g, '\\"');
//         return `"${escaped}"`;
//       });
//       csvRows.push(values.join(","));
//     }
//     return csvRows.join("\n");

//    };
//   const download = (arrayData) => {
//     const blob = new Blob([arrayData], { type: "text/csv" });
//     const url = window.URL.createObjectURL(blob);
//     const a = document.createElement("a");
//     a.setAttribute("hidden", "");
//     a.setAttribute("href", url);
//     a.setAttribute("download", "download.csv");
//     document.body.appendChild(a);
//     a.click();
//     document.body.removeChild(a);
//   };

//   //   //form escaped comma separated values
//   const csvData = objectToCsv(arrayData);
