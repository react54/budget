import React from "react";
import { UserContext } from "../context/UserProvider";
import { withRouter } from "react-router-dom";

const Login = (props) => {
  const { signIn, user } = React.useContext(UserContext);

  // const [load, setLoad] = React.useState(false);

  // const active = user.activo;
  // const loading = dataInit.loading;

  React.useEffect(() => {
    // console.log(active);

    if (user.estado) {
      props.history.push("/admin");
    }
  }, [user.estado, props.history]);

  return (
    <div className="main">
      <div className="box">
        <h1>Budget App</h1>
        <hr />
        <p>An app where you can track your incomes and expenses.</p>
        <button
          onClick={() => signIn()}
          className="btn btn-dark"
          disabled={user.estado}
        >
          Login with Goolge
        </button>
      </div>
    </div>
  );
};

export default withRouter(Login);
