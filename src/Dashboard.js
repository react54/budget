import React from "react";
import "./App.css";
import Balance from "./Components/Balance";
import ExpenseList from "./Components/ExpenseList";
import AddTransaction from "./Components/AddTransaction";
import SearchBox from "./Components/SearchBox";
import EditData from "./Components/EditData";

const Dashboard = (props) => {
  const [editing, setEditing] = React.useState(false);

  const [currentTran, setCurrentTran] = React.useState({
    id: null,
    order: "",
    date: "",
    concept: "",
    amount: null,
    type: "",
  });

  return (
    <div>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col text-center mt-3">
            <Balance></Balance>
          </div>
        </div>
        <hr></hr>
        <SearchBox></SearchBox>
        <hr></hr>
        <div className="row">
          <div className="col">
            <ExpenseList
              setCurrentTran={setCurrentTran}
              setEditing={setEditing}
            ></ExpenseList>
          </div>
        </div>

        <div className="row">
          <div className="col">
            {editing ? (
              <EditData
                currentTran={currentTran}
                setEditing={setEditing}
              ></EditData>
            ) : (
              <AddTransaction></AddTransaction>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
